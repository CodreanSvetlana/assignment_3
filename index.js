
const FIRST_NAME = "Svetlana";
const LAST_NAME = "Codrean";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
    getDetails() {
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience = "JUNIOR") {
       super(name, surname, salary);
       this.experience = experience;
   }

    applyBonus() {
        if(this.experience == "JUNIOR")
            return this.salary*1.1;
        else if(this.experience == "MIDDLE")
            return this.salary*1.15;
        else if(this.experience == "SENIOR")
            return this.salary*1.2;
        else
            return this.salary*1.1;
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

